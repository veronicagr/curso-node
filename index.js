var restify = require('restify');
var sql = require('mssql');
var Request = require('tedious');

function respond(req, res, next) {
  res.send('hello ' + req.params.name);
  next();
}

var server = restify.createServer();
server.get('/hello/:name', respond);
server.head('/hello/:name', respond);

server.listen(8080, function () {
  console.log('servidor rodando');
});

const config = {
  user: 'sa',
  password: 'Avanade@2018',
  server: 'localhost', // You can use 'localhost\\instance' to connect to named instance
  database: 'playlist',
  port: '8080',
  dialect: 'mssql',
  dialectOptions: {
  instanceName: "SQLEXPRESS"
}}



async function teste() {
  try {
    let pool = await sql.connect(config);
    let result1 = await pool.request()
      .input('input_parameter', sql.Int, value)
      .query('select * from playlist where id = @input_parameter');

    console.dir(result1)

  }catch(err){
    console.log(err);
  }
}

teste();  
